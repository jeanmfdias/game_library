from flask import Flask, render_template, request, redirect, flash, session, url_for

class Game:
    def __init__(self, name, description, platform):
        self.name=name
        self.description=description
        self.platform=platform


class User:
    def __init__(self, name, username, password):
        self.name = name
        self.username = username
        self.password = password

app = Flask(__name__)
app.secret_key = 'project1'

game1 = Game('GTA', 'Game 1', 'PC')
game2 = Game('NFS', 'Need For Speed', 'PC')
listGames = [game1, game2]

user1 = User('Jean', 'jn', 'admin')
user2 = User('Vanessa', 'va', 'admin')
users = { user1.username: user1, user2.username: user2 }

@app.get('/')
def index():
    return render_template('index.html', title='Games', games=listGames)

@app.route('/create')
def create():
    if 'user_logged' not in session or session['user_logged'] == None:
        return redirect(url_for('login', next=url_for('create')))
    return render_template('create.html', title='Create Game')

@app.route('/store', methods=['POST',])
def store():
    name = request.form['name']
    description = request.form['description']
    platform = request.form['platform']
    game = Game(name, description, platform)
    listGames.append(game)
    return redirect(url_for('index'))

@app.route('/login')
def login():
    next = request.args.get('next')
    if next is None:
        next = url_for('index')
    return render_template('login.html', title='Login', next=next)

@app.route('/signin', methods=['POST',])
def signin():
    if request.form['username'] in users:
        user = users[request.form['username']]
        if request.form['password'] == user.password:
            session['user_logged'] = request.form['username']
            flash(session['user_logged'] + ' logged with success')
            next_page = request.form['next']
            return redirect(next_page)
    flash('User don\'t logged.')
    return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session['user_logged'] = None
    flash('User logout done')
    return redirect(url_for('index'))

app.run(debug=True)